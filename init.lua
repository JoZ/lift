lift = {
	elevator_speed = 10.0
}

local function deduplicate(arr)
	local result = {}
	for key, value in ipairs(arr) do
		if value ~= arr[key + 1] then
			table.insert(result, value)
		end
	end
	return result
end

-- List of floor positions
function lift.find_floors(position, direction)
	local floors = {}
	local pos = vector.new(position)
	local node = minetest.get_node(pos)
	local node_floor_pos = minetest.find_node_near(pos, 1, { "lift:floor" })
	print(node.name)
	while node.name == "default:ladder_steel" do
		pos.y = pos.y + direction
		node = minetest.get_node(pos)
		node_floor_pos = minetest.find_node_near(pos, 1, { "lift:floor" })
		if node_floor_pos ~= nil then
			table.insert(floors, node_floor_pos.y)
		end
	end
	return floors
end

lift.elevator = {
	initial_properties = {
		visual = "mesh",
		mesh = "elevator.x",
		weight = 50,
		visual_size = { x = 1, y = 1, z = 1 },
		physical = true,
		collisionbox = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
		makes_footstep_sound = true,
		textures = { "elevator.png" }
	}
}

function lift.elevator.fly_to(self, y, with_who)
	local liftposy = self.object:get_pos().y
	local diff = y - liftposy
	local time = 0
	if diff > 0 then
		self.object:set_velocity({ x = 0, y = lift.elevator_speed, z = 0 })
		time = diff / lift.elevator_speed
	elseif diff < 0 then
		self.object:set_velocity({ x = 0, y = -lift.elevator_speed, z = 0 })
		time = -diff / lift.elevator_speed
	end


	with_who:set_attach(self.object, "", { x = 0, y = 0, z = 0 }, { x = 0, y = 0, z = 0 })

	minetest.after(time, function()
		self.object:set_velocity({ x = 0, y = 0, z = 0 })
		with_who:set_detach()
	end)
end

function lift.elevator.on_rightclick(self, clicker)
	local input = 1
	if clicker:get_attach() then
		clicker:set_detach()
	else
		local pos = vector.round(self.object:getpos())
		local floors = lift.find_floors(pos, -1)
		table.sort(floors)
		table.insert_all(floors, lift.find_floors(pos, 1))
		floors = deduplicate(floors)
		minetest.log("debug", dump(floors))
		local formspec = "size[4,8]textlist[1,0;2,0.5;dialog;Select floor]"
		local height = 0
		local width = 0
		for i in ipairs(floors)
		do
			width = 2
			if i % 2 == 1
			then
				height = height + 1
				width = 1
			end
			formspec = formspec .. "button_exit[" .. width .. "," .. height .. ";1,1;" .. i .. ";" .. i .. "]"
		end
		minetest.show_formspec(clicker:get_player_name(), "floor", formspec)

		minetest.register_on_player_receive_fields(function(clicker, formname, fields)
			minetest.log("debug", dump(fields))
			for k = 1, #floors
			do
				if fields[tostring(k)]
				then
					lift.elevator.fly_to(self, floors[k], clicker)
				end
			end
		end)
	end
end

minetest.register_entity("lift:elevator", lift.elevator)

minetest.register_node("lift:floor", {
	description = "floor",
	tiles = { "elevator_floor.png" },
	groups = { crumbly = 3, soil = 1 },
	drop = "lift:floor",
})


function lift.elevator.on_place(itemstack, placer, pointed_thing)
	local node = minetest.get_node(pointed_thing.under)
	if node.name == "default:ladder_steel" then
		elevator = minetest.add_entity(pointed_thing.under, "lift:elevator")
	else
		print(node.name)
	end
end

minetest.register_craftitem("lift:elevator", {
	description = "Elevator",
	wield_image = "elevator_side.png",
	inventory_image = minetest.inventorycube("elevator_top.png", "elevator_side.png", "elevator_side.png"),
	on_place = lift.elevator.on_place
})


minetest.register_craft({
	output = "lift:elevator",
	recipe = {
		{ '',                     '',                     '' },
		{ 'default:copper_ingot', 'default:mese_crystal', 'default:copper_ingot' },
		{ 'default:steel_ingot',  'default:steel_ingot',  'default:steel_ingot' },
	},
})

minetest.register_craft({
	output = "lift:floor",
	recipe = {
		{ 'default:wood', 'default:wood',        'default:wood' },
		{ 'default:wood', 'default:steel_ingot', 'default:wood' },
		{ 'default:wood', 'default:wood',        'default:wood' },
	},
})
