# lift
Mod for for Minetest that adds an elevator.
## Installation
Clone this repository into your `mods/` folder.
## Crafting

A '1x1 Elevator' is crafted out of 2 copper ingots, 1 mese crystal and 3 steel ingots.

|           |            |           |
|-----------|------------|-----------|
| ![Copper] | ![Mese]    | ![Copper] |
| ![Steel]  | ![Steel]   | ![Steel]  |


A '1x1 Floor' is crafted out of 8 wooden planks and 1 steel ingot.

|           |            |           |
|-----------|------------|-----------|
| ![Wood]   | ![Wood]    | ![Wood]   |
| ![Wood]   | ![Steel]   | ![Wood]   |
| ![Wood]   | ![Wood]    | ![Wood]   |

## Setup
While holding the crafted item, rightclick on a ladder.

![Lift1](screenshot1.png)
![Lift2](screenshot2.png)
![Lift3](screenshot3.png)

## Usage
Rightclick on the elevator. It will show you a formspec to select the floor you will be going to.

To detach, rightclick again.


[copper]: https://wiki.minetest.net/images/thumb/0/00/Copper_Ingot.png/48px-Copper_Ingot.png "Copper Ingot"
[Mese]: https://wiki.minetest.net/images/thumb/7/7b/Mese_Crystal.png/48px-Mese_Crystal.png "Mese"
[Steel]: https://wiki.minetest.net/images/thumb/b/bd/Steel_Ingot.png/48px-Steel_Ingot.png "Steel Ingot"
[Wood]: https://wiki.minetest.net/images/thumb/e/e6/Wooden_Planks.png/48px-Wooden_Planks.png "Wood"

